﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;

public class CreateRing : MonoBehaviour
{
    public GameObject ringPrefab;
    public Color correctRingColor;

    bool previousWasTouching = false;

    public void InstantiateRing()
    {
        Vector3 positionOnWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        positionOnWorld.z = 0;
        Instantiate(ringPrefab, positionOnWorld, Quaternion.identity);
    }

 
    // Start is called before the first frame update
    void Start()
    {
        Input.simulateMouseWithTouches = false;
    }

    // Update is called once per frame
    void Update()
    {
        bool isTouching = Input.touchCount > 0;
        if (isTouching && !previousWasTouching) InstantiateRing();
        previousWasTouching = isTouching;
        //CONTROL DE INPUT
        if (Input.GetMouseButtonDown(0))
        {
 
            InstantiateRing();
        }

    }
}
