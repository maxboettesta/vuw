﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineFromNod : MonoBehaviour
{
    private NodPathGenerator mainGenerationScript;
    public GameObject winUI;
    public LineRenderer linePrefab;
    private LineRenderer myLine;
    private NodPathGenerator Path;
    private Vector3 mouseLocation;

    private List<GameObject> gameObjectNods = new List<GameObject>();
    private List<Vector3> selectedNodsPositions = new List<Vector3>();


    public GameObject winUIReference;

    public IEnumerator DrawLineFromNodToMouse()
    {
        if (myLine != null)
        {
            //ACTUALIZA LA ULTIMA POSICION DONDE SE ENCUENTRA EL MOUSE
            mouseLocation.z = -1;
            myLine.SetPosition(myLine.positionCount-1, mouseLocation);
            
            //ITERA ENTRE TODOS LOS PUNTOS DE LA LINEA Y LOS ACTUALIZA
            for (int i = 0; i < myLine.positionCount-2; i++)
            {
                //offset de -1
                selectedNodsPositions[i] = new Vector3(selectedNodsPositions[i].x, selectedNodsPositions[i].y, -1);
        
                myLine.SetPosition(i, selectedNodsPositions[i]);
            }
            

            yield return new WaitForSeconds(0.01f);
            StartCoroutine(DrawLineFromNodToMouse());
        }
    }

    private void CheckWin()
    {
        bool possibleWin = false;
        foreach(GameObject nod in gameObjectNods)
        {
            if (nod != null)
            {
                if (!nod.GetComponent<Nod>().selectedNod)
                {
                    possibleWin = false;
                    break;
                }
                else
                {
                    possibleWin = true;
                }
            }
            
        }

        if (possibleWin)
        {
            Instantiate(winUI, Vector3.zero, Quaternion.identity);
            DestroyerAll();
        }
    }

    private void DestroyerAll()
    {
        foreach (GameObject nod in gameObjectNods)
        {
            Destroy(nod.gameObject);
        }
        Destroy(myLine);
        
    }

    private void SpawnLine()
    {
        //SI EXISTE LA LINEA
        if (myLine == null)
        {
                foreach (GameObject nod in gameObjectNods)
                {

                mouseLocation.z = 0;
                Debug.Log("mousePosition: " + mouseLocation);

                    //EL NODO CONTIENE EL MOUSE
                    if(nod!= null)
                    {
                        if (nod.GetComponent<Collider2D>().bounds.Contains(mouseLocation))
                        {
                            //CREA UNA LINEA
                            myLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
                            myLine.SetPosition(0, mouseLocation);
                            myLine.SetPosition(1, mouseLocation);
                            nod.GetComponent<Nod>().selectedNod = true;
                            nod.GetComponent<Nod>().ChangeCOLOR();


                            selectedNodsPositions.Add(nod.transform.position);
                            break;
                        }
                    }
                    
                }   
        }
        else
        {
            foreach (GameObject nod in gameObjectNods)
            {
                //EL NODO NO HA SIDO PREVIAMENTE SELECCIONADO
                if (!nod.GetComponent<Nod>().selectedNod)
                {
                    mouseLocation.z = 0;
                    Debug.Log("mousePosition: " + mouseLocation);

                    //EL NODO CONTIENE EL MOUSE
                    if (nod.GetComponent<Collider2D>().bounds.Contains(mouseLocation))
                    {
                        //INCREMENTA TAMAÑO DE LA LINEA
                        myLine.positionCount++;
                        //SELECCIONAR EL NODO
                        nod.GetComponent<Nod>().selectedNod = true;
                        nod.GetComponent<Nod>().ChangeCOLOR();
                        selectedNodsPositions.Add(nod.transform.position);
                        break;
                    }
                   
                }
                
            }

            //DIBUJAR LA LINEA Y ACTUALIZARLA
            StartCoroutine(DrawLineFromNodToMouse());
        }
        

    }

    // Start is called before the first frame update
    void Start()
    {
        myLine = GetComponent<LineRenderer>();
        Path = GetComponent<NodPathGenerator>();
        mainGenerationScript = GetComponent<NodPathGenerator>();


    }

    // Update is called once per frame
    void Update()
    {
        CheckWin();
        gameObjectNods = Path.nodList;
        
        mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //INSTANCIAR LINEA
        if (Input.GetMouseButtonDown(0) || Input.touchCount > 0)
        {
            SpawnLine();

            if(mainGenerationScript.InstantiatedUI!= null)
            mainGenerationScript.InstantiatedUI.GetComponent<VisionTrys>().ammountOfTrys--;
        }
    }
}
