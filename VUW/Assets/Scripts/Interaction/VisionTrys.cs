﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisionTrys : MonoBehaviour
{
    public int maxTrys = 30;
    public int ammountOfTrys = 6;
    public Text numberOfTrys;
    public GameObject endGameUI;
    public bool finishedGame = false;
    void CheckFinishGame()
    {
        if (ammountOfTrys <= 0 && !finishedGame)
        {
            Instantiate(endGameUI, Vector3.zero,Quaternion.identity);
            finishedGame = true;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        numberOfTrys.text = Mathf.Clamp(ammountOfTrys,0, maxTrys).ToString();
        CheckFinishGame();
    }
}
