﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{
    public AudioSource button_source;
    private bool oneTimeLoadAgain;
    IEnumerator EndGameFade()
    {
        button_source.Play();
        GetComponent<Animator>().Play("FadeOutEnd");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }
    public void ReloadLevel()
    {
        if (!oneTimeLoadAgain)
        {
            StartCoroutine(EndGameFade());
            oneTimeLoadAgain = true;
        }
       
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
