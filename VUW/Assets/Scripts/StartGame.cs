﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{
    public NodPathGenerator gameManager;
    public bool menuHidden;
    public float startHideSpeed = 2.0f;
    private Image[] menuImages;
    private Animator[] menuAnimators;
    private Color HiddenColor = new Color(1, 1, 1, 0);

    public AudioClip playSound;
    private AudioSource myASource;

    IEnumerator hideMenuCoroutine()
    {
        menuHidden = true;
        foreach (Image menuImage in menuImages)
        {
            menuImage.color = Color.Lerp(menuImage.color, new Color(menuImage.color.r, menuImage.color.g, menuImage.color.b, 0), Time.deltaTime * startHideSpeed);
        }
        if (menuImages[3].color.a >= 0.01f)
        {
            yield return new WaitForSeconds(0.01f);
            StartCoroutine(hideMenuCoroutine());
        }
        else
        {
            gameManager.startGame = true;
            gameObject.SetActive(false); 
        }
        
    }

    public void HideMenu()
    {
        if (!menuHidden)
        {
            myASource.PlayOneShot(playSound);
            StartCoroutine(hideMenuCoroutine());

            foreach (Animator menuAnimator in menuAnimators)
            {
                menuAnimator.enabled = false;
            }
        }
        
    }

    public void ShowMenu()
    {
        foreach (Image menuSprite in menuImages)
        {
            menuSprite.color = Color.Lerp(menuSprite.color, new Color(menuSprite.color.r, menuSprite.color.g, menuSprite.color.b, 1), Time.deltaTime * startHideSpeed);
        }

        foreach (Animator menuAnimator in menuAnimators)
        {
            menuAnimator.enabled = true;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        myASource = GetComponent<AudioSource>();
        menuImages = GetComponentsInChildren<Image>();
        menuAnimators = GetComponentsInChildren<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        

    }
}
