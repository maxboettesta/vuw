﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodPathGenerator : MonoBehaviour
{
    public AudioClip nodGenerationSound;
    public GameObject UIprefab;
    public GameObject InstantiatedUI;
    public bool startGame;
    public GameObject nodPrefab;
    public List<GameObject> nodList = new List<GameObject>();

    public int difficulty = 3;
    public float timeToInspect = 3.0f;

    private float RangeX = 2.5f;
    private float RangeY = 4.0f;

    private AudioSource myAudioSource;


    public IEnumerator CreateNodPath()
    {
        for(int i =0; i < difficulty; i++)
        {
            GameObject currentNod = Instantiate(nodPrefab, new Vector3(Random.Range(RangeX, -RangeX), Random.Range(-RangeY, RangeY), 0), Quaternion.identity);
            currentNod.GetComponent<Nod>().InstantiatedUIfromNod = InstantiatedUI;
            nodList.Add(currentNod);
            //SFX
            myAudioSource.pitch = (1+i*0.1f);
            myAudioSource.PlayOneShot(nodGenerationSound);
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(timeToInspect);
        //ACTIVAR MASCARA
        foreach(GameObject nod in nodList)
        {
            if(nod!=null)
            nod.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (startGame)
        {
            //INICIO DE PARTIDA
            //CREAR HUD
            InstantiatedUI = Instantiate(UIprefab, Vector3.zero, Quaternion.identity);
            //CREAR NODOS
            StartCoroutine(CreateNodPath());
            startGame = false;
        }
    }
}
