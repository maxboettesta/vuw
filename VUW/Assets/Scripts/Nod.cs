﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nod : MonoBehaviour
{
    public GameObject InstantiatedUIfromNod;
    public bool selectedNod;
    private AudioSource nodAudio;
    private GameObject ComingNod;
    public Color NodColor;
    public Color SelectedNodColor;
    private enum NodType { Yellow , Green };
    private GameObject GoingNod;

    public void ChangeCOLOR()
    {
        InstantiatedUIfromNod.GetComponent<VisionTrys>().ammountOfTrys++;
        GetComponent<SpriteRenderer>().color = SelectedNodColor;
        GetComponent<Animator>().Play(0);
        nodAudio.Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().color = NodColor;
        nodAudio = GetComponent<AudioSource>();
    }
   
    // Update is called once per frame
    void Update()
    {
        
    }
}
